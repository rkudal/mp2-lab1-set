// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tbitfield.cpp - Copyright (c) Гергель В.П. 07.05.2001
// Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (19.04.2015)
//
// Битовое поле

#include "tbitfield.h"

// Получить размер TELEM в битах
inline const int TELEMbitSize()
{
	return sizeof(TELEM) * 8;
}

TBitField::TBitField(int len) : BitLen(len)
{
	if (BitLen < 0) throw length_error("Length has to be a non-negative value");
	int bitSize = TELEMbitSize();
	if (BitLen % bitSize)
		MemLen = BitLen / bitSize + 1;
	else
		MemLen = BitLen / bitSize;
	pMem = new TELEM[MemLen];
	for (int i = 0; i < MemLen; ++i)
		pMem[i] = 0;
}

TBitField::TBitField(const TBitField &bf) // Конструктор копирования
{
	BitLen = bf.BitLen;
	MemLen = bf.MemLen;
	pMem = new TELEM[MemLen];
	for (int i = 0; i < MemLen; ++i)
		pMem[i] = bf.pMem[i];
}

TBitField::~TBitField()
{
	if (pMem != nullptr)
		delete[] pMem;
}

int TBitField::GetMemIndex(const int n) const // Индекс Мем для бита n
{
	if (n < 0 || n >= BitLen)
		throw out_of_range("Bit is out of range");
	return n / TELEMbitSize();
}

TELEM TBitField::GetMemMask(const int n) const // Битовая маска для бита n
{
	if (n < 0 || n >= BitLen)
		throw out_of_range("Bit is out of range");
	TELEM mask = 1 << (n - GetMemIndex(n) * TELEMbitSize());
	return mask;
}

// Доступ к битам битового поля

int TBitField::GetLength(void) const // Получить длину (к-во битов)
{
	return BitLen;
}

void TBitField::SetBit(const int n) // Установить бит
{
	if (n < 0 || n >= BitLen)
		throw out_of_range("Bit is out of range");
	pMem[GetMemIndex(n)] |= GetMemMask(n);
}

void TBitField::ClrBit(const int n) // Очистить бит
{
	if (n < 0 || n >= BitLen)
		throw out_of_range("Bit is out of range");
	pMem[GetMemIndex(n)] &= ~GetMemMask(n);
}

int TBitField::GetBit(const int n) const // Получить значение бита
{
	if (n < 0 || n >= BitLen)
		throw out_of_range("Bit is out of range");
	if (pMem[GetMemIndex(n)] & GetMemMask(n))
		return 1;
	else return 0;
}

// Битовые операции

TBitField& TBitField::operator=(const TBitField &bf) // Присваивание
{
	if (this != &bf)
	{
		BitLen = bf.BitLen;
		if (MemLen != bf.MemLen)
		{
			MemLen = bf.MemLen;
			if (pMem != nullptr)
				delete[] pMem;
			pMem = new TELEM[MemLen];
		}
		for (int i = 0; i < MemLen; ++i)
			pMem[i] = bf.pMem[i];
	}
	return *this;
}

int TBitField::operator==(const TBitField &bf) const // Сравнение
{
	if (BitLen != bf.BitLen) return 0;
	for (int i = 0; i < MemLen; ++i)
		if (pMem[i] != bf.pMem[i]) return 0;
	return 1;
}

int TBitField::operator!=(const TBitField &bf) const // Сравнение
{
	if (BitLen != bf.BitLen) return 1;
	for (int i = 0; i < MemLen; ++i)
		if (pMem[i] != bf.pMem[i]) return 1;
	return 0;
}

TBitField TBitField::operator|(const TBitField &bf) // Операция "или"
{
	int greater_len;
	BitLen > bf.BitLen ? greater_len = BitLen : greater_len = bf.BitLen;
	TBitField tmp(greater_len);
	for (int i = 0; i < MemLen; ++i)
		tmp.pMem[i] = pMem[i];
	for (int i = 0; i < bf.MemLen; ++i)
		tmp.pMem[i] |= bf.pMem[i];
	return tmp;		
}

TBitField TBitField::operator&(const TBitField &bf) // Операция "и"
{
	int greater_bitLen, shorter_memLen;
	if (BitLen > bf.BitLen)
	{
		greater_bitLen = BitLen;
		shorter_memLen = bf.MemLen;
	}
	else
	{
		greater_bitLen = bf.BitLen;
		shorter_memLen = MemLen;
	}
	TBitField tmp(greater_bitLen);
	for (int i = 0; i < shorter_memLen; ++i)
		tmp.pMem[i] = pMem[i] & bf.pMem[i];
	return tmp;
}

TBitField TBitField::operator~(void) // Отрицание
{
	TBitField tmp(BitLen);
	for (int i = 0; i < BitLen; ++i)
		if (!GetBit(i)) tmp.SetBit(i);
	return tmp;
}

// Ввод/Вывод

istream &operator>>(istream &istr, TBitField &bf) // Ввод
{
	int i = 0; char bit;
	while (i < bf.BitLen)
	{
		istr >> bit;
		if (bit == '0') bf.ClrBit(i++);
		else if (bit == '1') bf.SetBit(i++);
		else break;
	}
	return istr;
}

ostream &operator<<(ostream &ostr, const TBitField &bf) // Вывод
{
	for (int i = 0; i < bf.BitLen; ++i)
		ostr << bf.GetBit(i);
	ostr << endl;
	return ostr;
}