// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tset.cpp - Copyright (c) Гергель В.П. 04.10.2001
// Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (19.04.2015)
//
// Множество - реализация через битовые поля

#include "tset.h"

TSet::TSet(int mp) : MaxPower(mp), BitField(mp) {}

// Конструктор копирования
TSet::TSet(const TSet &s) : MaxPower(s.MaxPower), BitField(s.BitField) {}

// Конструктор преобразования типа
TSet::TSet(const TBitField &bf) : MaxPower(bf.GetLength()), BitField(bf) {}

TSet::operator TBitField() { return TBitField(BitField); }

int TSet::GetMaxPower(void) const // Получить макс. к-во эл-тов
{
	return MaxPower;
}

int TSet::IsMember(const int Elem) const // Элемент множества?
{
    return BitField.GetBit(Elem);
}

void TSet::InsElem(const int Elem) // Включение элемента множества
{
	BitField.SetBit(Elem);
}

void TSet::DelElem(const int Elem) // Исключение элемента множества
{
	BitField.ClrBit(Elem);
}

// Теоретико-множественные операции

TSet& TSet::operator=(const TSet &s) // Присваивание
{
	if (this != &s)
	{
		BitField = s.BitField;
		MaxPower = s.MaxPower;
	}
	return *this;
}

int TSet::operator==(const TSet &s) const // Сравнение
{
    return BitField == s.BitField;
}

int TSet::operator!=(const TSet &s) const // Сравнение
{
	return BitField != s.BitField;
}

TSet TSet::operator+(const TSet &s) // Объединение
{
	return TSet(BitField | s.BitField);
}

TSet TSet::operator+(const int Elem) // Объединение с элементом
{
	TSet tmp(*this);
	tmp.InsElem(Elem);
	return tmp;
}

TSet TSet::operator-(const int Elem) // Разность с элементом
{
	TSet tmp(*this);
	tmp.DelElem(Elem);
	return tmp;
}

TSet TSet::operator*(const TSet &s) // Пересечение
{
	return TSet(BitField & s.BitField);
}

TSet TSet::operator~(void) // Дополнение
{
	return TSet(~BitField);
}

// Перегрузка ввода/вывода

istream &operator>>(istream &istr, TSet &s) // Ввод
{
	int i = 0, value;
	while (i < s.MaxPower)
	{
		// Проверка состояния потока. Соответствует !fail().
		if (istr >> value)
			if (!s.IsMember(value))
			{
				s.InsElem(value);
				i++;
			}
			else continue;
		else break;
	}
	return istr;
}

ostream& operator<<(ostream &ostr, const TSet &s) // Вывод
{
	ostr << "Set: ";
	for (int i = 0; i < s.MaxPower; ++i)
		if (s.IsMember(i)) ostr << i << " ";
	ostr << endl;
	return ostr;
}