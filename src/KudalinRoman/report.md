# Множества на основе битовых полей


### Постановка задачи

Целью данной работы является разработка структуры данных для хранения множеств с
использованием битовых полей, а также освоение таких инструментов разработки
программного обеспечения, как система контроля версий [Git](https://git-scm.com/book/ru/v2) и фрэймворк для
разработки автоматических тестов [Google Test](https://github.com/google/googletest) для проведения модульного тестирования программы.


Выполнение работы предполагает решение следующих задач:

  1. Реализация класса битового поля `TBitField` согласно заданному интерфейсу.
  1. Реализация класса множества `TSet` согласно заданному интерфейсу.
  1. Обеспечение работоспособности тестов и примера использования ([Решето Эратосфена](https://habrahabr.ru/post/91112)).
  1. Реализация нескольких простых тестов на базе [Google Test](https://github.com/google/googletest).
  1. Публикация исходных кодов в личном репозитории на [Bitbucket](https://bitbucket.org).


### Использовавшиеся инструменты

При выполнении работы использовались следующие инструменты:

  * Система контроля версий [Git](https://git-scm.com/downloads).
  * Фреймворк для написания автоматических тестов [Google Test](https://github.com/google/googletest).
  * IDE - [Microsoft Visual Studio 2017 Community Edition](https://www.visualstudio.com/ru/downloads/?rr=https%3A%2F%2Fwww.google.ru%2F).
  * Язык программирования - C++.


### Реализация класса `TBitField`

Ниже представлены исходные коды объявления класса `TBitField` и реализации его методов:

`TBitField.h`
```
// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tbitfield.h - Copyright (c) Гергель В.П. 07.05.2001
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (19.04.2015)
//
// Битовое поле

#ifndef __BITFIELD_H__
#define __BITFIELD_H__

#include <iostream>

using namespace std;

typedef unsigned int TELEM;

class TBitField
{
private:
  int  BitLen; // длина битового поля - макс. к-во битов
  TELEM *pMem; // память для представления битового поля
  int  MemLen; // к-во эл-тов Мем для представления бит.поля

  // методы реализации
  int   GetMemIndex(const int n) const; // индекс в pМем для бита n       (#О2)
  TELEM GetMemMask (const int n) const; // битовая маска для бита n       (#О3)
public:
  TBitField(int len);                //                                   (#О1)
  TBitField(const TBitField &bf);    //                                   (#П1)
  ~TBitField();                      //                                    (#С)

  // доступ к битам
  int GetLength(void) const;      // получить длину (к-во битов)           (#О)
  void SetBit(const int n);       // установить бит                       (#О4)
  void ClrBit(const int n);       // очистить бит                         (#П2)
  int  GetBit(const int n) const; // получить значение бита               (#Л1)

  // битовые операции
  int operator==(const TBitField &bf) const; // сравнение                 (#О5)
  int operator!=(const TBitField &bf) const; // сравнение
  TBitField& operator=(const TBitField &bf); // присваивание              (#П3)
  TBitField  operator|(const TBitField &bf); // операция "или"            (#О6)
  TBitField  operator&(const TBitField &bf); // операция "и"              (#Л2)
  TBitField  operator~(void);                // отрицание                  (#С)

  friend istream &operator>>(istream &istr, TBitField &bf);       //      (#О7)
  friend ostream &operator<<(ostream &ostr, const TBitField &bf); //      (#П4)
};
// Структура хранения битового поля
//   бит.поле - набор битов с номерами от 0 до BitLen
//   массив pМем рассматривается как последовательность MemLen элементов
//   биты в эл-тах pМем нумеруются справа налево (от младших к старшим)
// О8 Л2 П4 С2

#endif
```

`TBitField.cpp`
```
// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tbitfield.cpp - Copyright (c) Гергель В.П. 07.05.2001
// Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (19.04.2015)
//
// Битовое поле

#include "tbitfield.h"

// Получить размер TELEM в битах
inline const int TELEMbitSize()
{
	return sizeof(TELEM) * 8;
}

TBitField::TBitField(int len) : BitLen(len)
{
	if (BitLen < 0) throw length_error("Length has to be a non-negative value");
	int bitSize = TELEMbitSize();
	if (BitLen % bitSize)
		MemLen = BitLen / bitSize + 1;
	else
		MemLen = BitLen / bitSize;
	pMem = new TELEM[MemLen];
	for (int i = 0; i < MemLen; ++i)
		pMem[i] = 0;
}

TBitField::TBitField(const TBitField &bf) // Конструктор копирования
{
	BitLen = bf.BitLen;
	MemLen = bf.MemLen;
	pMem = new TELEM[MemLen];
	for (int i = 0; i < MemLen; ++i)
		pMem[i] = bf.pMem[i];
}

TBitField::~TBitField()
{
	if (pMem != nullptr)
		delete[] pMem;
}

int TBitField::GetMemIndex(const int n) const // Индекс Мем для бита n
{
	if (n < 0 || n >= BitLen)
		throw out_of_range("Bit is out of range");
	return n / TELEMbitSize();
}

TELEM TBitField::GetMemMask(const int n) const // Битовая маска для бита n
{
	if (n < 0 || n >= BitLen)
		throw out_of_range("Bit is out of range");
	TELEM mask = 1 << (n - GetMemIndex(n) * TELEMbitSize());
	return mask;
}

// Доступ к битам битового поля

int TBitField::GetLength(void) const // Получить длину (к-во битов)
{
	return BitLen;
}

void TBitField::SetBit(const int n) // Установить бит
{
	if (n < 0 || n >= BitLen)
		throw out_of_range("Bit is out of range");
	pMem[GetMemIndex(n)] |= GetMemMask(n);
}

void TBitField::ClrBit(const int n) // Очистить бит
{
	if (n < 0 || n >= BitLen)
		throw out_of_range("Bit is out of range");
	pMem[GetMemIndex(n)] &= ~GetMemMask(n);
}

int TBitField::GetBit(const int n) const // Получить значение бита
{
	if (n < 0 || n >= BitLen)
		throw out_of_range("Bit is out of range");
	if (pMem[GetMemIndex(n)] & GetMemMask(n))
		return 1;
	else return 0;
}

// Битовые операции

TBitField& TBitField::operator=(const TBitField &bf) // Присваивание
{
	if (this != &bf)
	{
		BitLen = bf.BitLen;
		if (MemLen != bf.MemLen)
		{
			MemLen = bf.MemLen;
			if (pMem != nullptr)
				delete[] pMem;
			pMem = new TELEM[MemLen];
		}
		for (int i = 0; i < MemLen; ++i)
			pMem[i] = bf.pMem[i];
	}
	return *this;
}

int TBitField::operator==(const TBitField &bf) const // Сравнение
{
	if (BitLen != bf.BitLen) return 0;
	for (int i = 0; i < MemLen; ++i)
		if (pMem[i] != bf.pMem[i]) return 0;
	return 1;
}

int TBitField::operator!=(const TBitField &bf) const // Сравнение
{
	if (BitLen != bf.BitLen) return 1;
	for (int i = 0; i < MemLen; ++i)
		if (pMem[i] != bf.pMem[i]) return 1;
	return 0;
}

TBitField TBitField::operator|(const TBitField &bf) // Операция "или"
{
	int greater_len;
	BitLen > bf.BitLen ? greater_len = BitLen : greater_len = bf.BitLen;
	TBitField tmp(greater_len);
	for (int i = 0; i < MemLen; ++i)
		tmp.pMem[i] = pMem[i];
	for (int i = 0; i < bf.MemLen; ++i)
		tmp.pMem[i] |= bf.pMem[i];
	return tmp;		
}

TBitField TBitField::operator&(const TBitField &bf) // Операция "и"
{
	int greater_bitLen, shorter_memLen;
	if (BitLen > bf.BitLen)
	{
		greater_bitLen = BitLen;
		shorter_memLen = bf.MemLen;
	}
	else
	{
		greater_bitLen = bf.BitLen;
		shorter_memLen = MemLen;
	}
	TBitField tmp(greater_bitLen);
	for (int i = 0; i < shorter_memLen; ++i)
		tmp.pMem[i] = pMem[i] & bf.pMem[i];
	return tmp;
}

TBitField TBitField::operator~(void) // Отрицание
{
	TBitField tmp(BitLen);
	for (int i = 0; i < BitLen; ++i)
		if (!GetBit(i)) tmp.SetBit(i);
	return tmp;
}

// Ввод/Вывод

istream &operator>>(istream &istr, TBitField &bf) // Ввод
{
	int i = 0; char bit;
	while (i < bf.BitLen)
	{
		istr >> bit;
		if (bit == '0') bf.ClrBit(i++);
		else if (bit == '1') bf.SetBit(i++);
		else break;
	}
	return istr;
}

ostream &operator<<(ostream &ostr, const TBitField &bf) // Вывод
{
	for (int i = 0; i < bf.BitLen; ++i)
		ostr << bf.GetBit(i);
	ostr << endl;
	return ostr;
}
```

В качестве битового поля используется массив элементов типа `TELEM`, где `TELEM` - синоним для некоторого беззнакового типа (в данной реализации используется `unsigned int`).

Так как основой битового поля является массив, появляется необходимость в доступе к отдельным битам определенного элемента `TELEM`. В связи с этим возникает ряд вопросов:

  1. Какое количесвто элементов `TELEM` необходимо для представления битового поля длиной `n`?
  2. Как обратиться к определенному биту?
  3. Какие операции должны быть реализованы между битовыми полями, чтобы на их основе можно было реализовать множество?


**1. Подсчет количества элементов `TELEM`.**

В языке C++ имеется функция `sizeof()`, возвращающая размер типа, переданного ей в качестве параметра, в байтах. Воспользовавшись соотношением `1 Байт = 8 бит`, получим, что в элементе `TELEM` содержится `sizeof(TELEM) * 8` бит. Таким образом, если `len` без остатка делится на размер `TELEM` в битах, то для хранения поля длиной `len` потребуется ровно `len / (sizeof(TELEM) * 8)` элементов `TELEM`. Очевидно, что в противном случае потребуется на один элемент больше.

**2. Обращение к биту `n`.**

Для обращения к биту `n` необходимо решить три задачи:

  1. Выяснить индекс элемента `TELEM`, в котором располагается данный бит.
  2. Определить смещение в этом элементе.
  3. Составить битовую маску.

В силу того, что элементы массива нумеруются от `0` до `N - 1`, для решения первой задачи достаточно воспользоваться формулой `n / (sizeof(TELEM) * 8)`. Эта возможность реализована в методе `GetMemIndex()`.

Определив индекс элемента `TELEM`, необходимо выяснить, с какого числа начинается нумерация битов в данном элементе. Очевидно, для этого достаточно умножить вычесленный индекс на размер `TELEM` в битах. Тогда смещение легко может быть найдено по формуле `n - GetMemIndex(n) * sizeof(TELEM) * 8`. Назовем эту величину `offset`.

Последний шаг - составить *битовую маску*. Маска - это определенные данные, использующиеся для выбора отдельных битов из двоичной строки. Например, если нас интересует второй бит числа `01101110`, маска для него будет иметь следующий вид: `00100000` (нумерация битов слева направо). Таким образом, зная `offset` (т.е. местоположение интересующего нас бита в данном элементе), нетрудно составить маску: для этого достаточно задать начальное значение `1` и сместить эту единицу на величину `offset`. Таким образом, искомая формула получения маски для бита `n` имеет вид: `1 << (n - GetMemIndex(n) * sizeof(TELEM) * 8)`.

Зная маску, можно *получить значение бита*, применив поразрядную [конъюнкцию](https://ru.wikipedia.org/wiki/%D0%91%D0%B8%D1%82%D0%BE%D0%B2%D1%8B%D0%B5_%D0%BE%D0%BF%D0%B5%D1%80%D0%B0%D1%86%D0%B8%D0%B8#AND) `&`. *Установить бит* можно с помощью поразрядной [дизъюнкции](https://ru.wikipedia.org/wiki/%D0%91%D0%B8%D1%82%D0%BE%D0%B2%D1%8B%D0%B5_%D0%BE%D0%BF%D0%B5%D1%80%D0%B0%D1%86%D0%B8%D0%B8#OR) `|`, а *сбросить значение* - применив поразрядную конъюнкцию `&` с инвертированной маской.

**3. Операции над битовыми полями**.

Как было указано выше, цель проекта - разработка множества на основе битового поля. Поэтому все операции, которые должны быть реализованы над множествами, должны поддерживаться и битовыми полями.

В проекте имеются следующие теоретико-множественные операции:

  * Присваивание
  * Сравнение
  * Объединение
  * Пересечение
  * Дополнение

Работа соответсвующих битовых операций обеспечена для класса `TBitField`.

Ниже представлена таблица всех операций над множествами и битовыми полями:

| Оператор |               Множество              |        Битовое поле        |
|:--------:|:------------------------------------:|:--------------------------:|
|     =    |             Присваивание             |        Присваивание        |
|  ==, !=  |               Сравнение              |          Сравнение         |
|     +    | Объединение, объединение с элементом |                            |
|     -    |         Разность с элементом         |                            |
|     *    |              Пересечение             |                            |
|     ~    |              Дополнение              | Отрицание (инвертирование) |
|     I    |                                      |       Операция "или"       |
|     &    |                                      |        Операция "и"        |

### Реализация класса `TSet`

Ниже представлены исходные коды объявления класса `TSet` и реализации его методов:

`TSet.h`
```
// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tset.h - Copyright (c) Гергель В.П. 07.05.2001
//   Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (19.04.2015)
//
// Множество

#ifndef __SET_H__
#define __SET_H__

#include "tbitfield.h"

class TSet
{
private:
  int MaxPower;       // максимальная мощность множества
  TBitField BitField; // битовое поле для хранения характеристического вектора
public:
  TSet(int mp);
  TSet(const TSet &s);       // конструктор копирования
  TSet(const TBitField &bf); // конструктор преобразования типа
  operator TBitField();      // преобразование типа к битовому полю
  // доступ к битам
  int GetMaxPower(void) const;     // максимальная мощность множества
  void InsElem(const int Elem);       // включить элемент в множество
  void DelElem(const int Elem);       // удалить элемент из множества
  int IsMember(const int Elem) const; // проверить наличие элемента в множестве
  // теоретико-множественные операции
  int operator== (const TSet &s) const; // сравнение
  int operator!= (const TSet &s) const; // сравнение
  TSet& operator=(const TSet &s);  // присваивание
  TSet operator+ (const int Elem); // объединение с элементом
                                   // элемент должен быть из того же универса
  TSet operator- (const int Elem); // разность с элементом
                                   // элемент должен быть из того же универса
  TSet operator+ (const TSet &s);  // объединение
  TSet operator* (const TSet &s);  // пересечение
  TSet operator~ (void);           // дополнение

  friend istream &operator>>(istream &istr, TSet &bf);
  friend ostream &operator<<(ostream &ostr, const TSet &bf);
};
#endif
```
`TSet.cpp`
```
// ННГУ, ВМК, Курс "Методы программирования-2", С++, ООП
//
// tset.cpp - Copyright (c) Гергель В.П. 04.10.2001
// Переработано для Microsoft Visual Studio 2008 Сысоевым А.В. (19.04.2015)
//
// Множество - реализация через битовые поля

#include "tset.h"

TSet::TSet(int mp) : MaxPower(mp), BitField(mp) {}

// Конструктор копирования
TSet::TSet(const TSet &s) : MaxPower(s.MaxPower), BitField(s.BitField) {}

// Конструктор преобразования типа
TSet::TSet(const TBitField &bf) : MaxPower(bf.GetLength()), BitField(bf) {}

TSet::operator TBitField() { return TBitField(BitField); }

int TSet::GetMaxPower(void) const // Получить макс. к-во эл-тов
{
	return MaxPower;
}

int TSet::IsMember(const int Elem) const // Элемент множества?
{
    return BitField.GetBit(Elem);
}

void TSet::InsElem(const int Elem) // Включение элемента множества
{
	BitField.SetBit(Elem);
}

void TSet::DelElem(const int Elem) // Исключение элемента множества
{
	BitField.ClrBit(Elem);
}

// Теоретико-множественные операции

TSet& TSet::operator=(const TSet &s) // Присваивание
{
	if (this != &s)
	{
		BitField = s.BitField;
		MaxPower = s.MaxPower;
	}
	return *this;
}

int TSet::operator==(const TSet &s) const // Сравнение
{
    return BitField == s.BitField;
}

int TSet::operator!=(const TSet &s) const // Сравнение
{
	return BitField != s.BitField;
}

TSet TSet::operator+(const TSet &s) // Объединение
{
	return TSet(BitField | s.BitField);
}

TSet TSet::operator+(const int Elem) // Объединение с элементом
{
	TSet tmp(*this);
	tmp.InsElem(Elem);
	return tmp;
}

TSet TSet::operator-(const int Elem) // Разность с элементом
{
	TSet tmp(*this);
	tmp.DelElem(Elem);
	return tmp;
}

TSet TSet::operator*(const TSet &s) // Пересечение
{
	return TSet(BitField & s.BitField);
}

TSet TSet::operator~(void) // Дополнение
{
	return TSet(~BitField);
}

// Перегрузка ввода/вывода

istream &operator>>(istream &istr, TSet &s) // Ввод
{
	int i = 0, value;
	while (i < s.MaxPower)
	{
		// Проверка состояния потока. Соответствует !fail().
		if (istr >> value)
			if (!s.IsMember(value))
			{
				s.InsElem(value);
				i++;
			}
			else continue;
		else break;
	}
	return istr;
}

ostream& operator<<(ostream &ostr, const TSet &s) // Вывод
{
	ostr << "Set: ";
	for (int i = 0; i < s.MaxPower; ++i)
		if (s.IsMember(i)) ostr << i << " ";
	ostr << endl;
	return ostr;
}
```
Класс множества реализован посредством битового поля, поэтому мы применяем *композицию*, т.е. класс `TSet` содержит экземпляр класса `TBitField`. Такая реализация приводит к следующему отношению: элемент `i` является членом множества тогда и только тогда, когда в битовом поле установлен бит `i`. Например, если элемент `50` является членом множества, то бит номер `50` утсановлен в поле.

Такое отношение позволяет имплементировать теоретико-множественные операции на основе уже написанных битовых операций:

  * Присваивание - присваивание битовых полей и мощностей.
  * Сравнение - сравнение битовых полей.
  * Объединение - операция "или" для битовых полей.
  * Пересечение - операция "и" для битовых полей.
  * Дополнение - инвертирование битового поля.
  * Вставка элемента `n` - установка бита `n`.
  * Удаление элемента `n` - сброс бита `n`.
  * Проверка элемента `n` на принадлежность множеству - получение значения бита `n`.

### Результаты тестирования

Классы `TSet` и `TBitField` были протестированы с помощью наборов тестов на базе [Google Test](https://github.com/google/googletest). Результаты представлены ниже:

  * Результаты тестов класса `TBitField`:
![TBitField test](https://bytebucket.org/f3nom3n/images/raw/d05ab112511d3615eec205c9789d34b1de259f8a/bitset_set/TBitField_TEST.jpg?token=94bb1b3e759d49d1f944cfb7efb934744ee61646)

  * Результаты тестов класса `TSet`:
![TSet test](https://bytebucket.org/f3nom3n/images/raw/d05ab112511d3615eec205c9789d34b1de259f8a/bitset_set/TSet_TEST.jpg?token=7a92ef9c281f4769442c4e3e9eddceb732ea364f)

  * Результаты теста приложения "Решето Эратосфена" с использованием битового поля:
![Sieve using bitfield](https://bytebucket.org/f3nom3n/images/raw/d05ab112511d3615eec205c9789d34b1de259f8a/bitset_set/Sieve_using_bitfield_TEST.jpg?token=8d952ab1f70ec4eee74ff4d311a2302cca4e4e46)

  * Результаты теста приложения "Решето Эратосфена" с использованием множества:
![Sieve using set](https://bytebucket.org/f3nom3n/images/raw/d05ab112511d3615eec205c9789d34b1de259f8a/bitset_set/Sieve_using_set_TEST.jpg?token=c12a0a59dc0cd59dda780f140e21a08155927be9)

### Приложение

С исходным кодом тестов можно ознакомиться по ссылкам:

  - [TBitField tests](https://bitbucket.org/f3nom3n/mp2-lab1-set/src/5b5b2e8627bad969f52198c14fe3b995efe70b27/src/KudalinRoman/test_tbitfield.cpp?at=master&fileviewer=file-view-default)
  - [TSet tests](https://bitbucket.org/f3nom3n/mp2-lab1-set/src/5b5b2e8627bad969f52198c14fe3b995efe70b27/src/KudalinRoman/test_tset.cpp?at=master&fileviewer=file-view-default)
